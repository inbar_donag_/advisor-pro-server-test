import json
import paramiko
import pymongo
from sshtunnel import SSHTunnelForwarder
from constants import testDMS, glooko

######################## connection variables ##################################


with open("./config/testsConfig.json") as json_data:
    config = json.load(json_data)

db = config["database"]
ssh = config["ssh"]
downloadedFilesLocation = config["downloadedFilesLocation"]

server = SSHTunnelForwarder(
    ssh["host"],
    ssh_username= ssh["username"],
    ssh_password= ssh["password"],
    remote_bind_address=(db["host"], db["port"])
)

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
mock1 = config["mock1"]
mock2 = config["mock2"]

##############################################################################

def initConnectionToDatabase():
    server.start()
    client = pymongo.MongoClient(db["host"], server.local_bind_port)
    return client[db["name"]]

print("\ninitializing connections")
db_client = initConnectionToDatabase()
ssh_client.connect(hostname=ssh["host"], username=ssh["username"], password=ssh["password"])

class ValidVars():
    __instance = None
    validRequestId = None
    validRequestId_glooko = None

    @staticmethod
    def getInstance():
        if ValidVars.__instance == None:
            ValidVars()
        return ValidVars.__instance

    def __init__(self):
        if ValidVars.__instance != None:
            raise Exception("This class is a singleton")
        else:
            ValidVars.__instance = self


    def setValidIDs(self, id_testDMS, id_glooko):
        self.validRequestId = id_testDMS
        self.validRequestId_glooko = id_glooko

    def getValidID(self, dms=testDMS):
        if dms == glooko: return self.validRequestId_glooko
        return self.validRequestId

