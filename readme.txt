
_____Usage Instructions_____

run everything (FVT)
pytest -v

run everything on <num> cores
pytest -v -n <num>

run BVT
pytest -v -m BVT

run specific STP-<num>:
pytest -v cases/test_<num>.py

run specific test method
pytest -v -k <method name>


_______Useful Options_______

add -s to show prints
add –lf to run only the set of tests that failed at the last run, or all tests if none failed.
add --durations=0 to show duration of each function
