import pathlib
import shutil
import pytest
from constants import *
from main import db_client, ssh_client, server, downloadedFilesLocation, ValidVars
from serverRequests.adsRequests import requestAndWaitForLastStatus, testClientToken, requestAndGetInputFile


@pytest.fixture(scope="session", autouse=True)
def my_own_session_run_at_beginning(request):
    pathlib.Path(downloadedFilesLocation).mkdir(exist_ok=True)

    print("\nsending valid requests")
    testDMS_ID = requestAndWaitForLastStatus(testClientToken, testDMS, validUser, syncTimestamps1, status_completed)
    glooko_ID = requestAndWaitForLastStatus(testClientToken, glooko, validUser, syncTimestamps1, status_completed)

    ValidVars.getInstance().setValidIDs(testDMS_ID, glooko_ID)

    def my_own_session_run_at_end():
        print("\nclosing connections")
        db_client.logout()
        ssh_client.close()
        server.stop()
        shutil.rmtree(downloadedFilesLocation)

    request.addfinalizer(my_own_session_run_at_end)

