import time
import requests
from constants import *
from filesHelper import getAlgoInputJson

with open("./config/testsConfig.json") as json_data:
    config = json.load(json_data)

ads = config["ads"]
testClientToken = ads["testClientToken"]
testClient2Token = ads["testClient2Token"]
adminToken = ads["adminToken"]
host = ads["host"]
port = ads["port"]
treatmentPlanRequestUrl = "http://" + host + ":" + port + '/api/v1/advisorPro/treatmentPlanRequest/'


def getMessageForTreatmentPlanRequest(dmsName, patientId, syncTimestamps):
    return {
        "dmsName": dmsName,
        "userId": patientId,
        "syncTimestamps": syncTimestamps
    }

def treatmentPlanRequest(authToken, dmsName, patientId, syncTimestamps):

    headers = {'authorization': authToken,
               'Content-Type': 'application/json'}
    try:
        return requests.post(treatmentPlanRequestUrl, headers = headers,
                             json=getMessageForTreatmentPlanRequest(dmsName,patientId,syncTimestamps))
    except requests.exceptions.ConnectionError:
        raise Exception(adsConnectionErr)

def requestStatus(authToken, requestId):
    headers = {'authorization': authToken}
    try:
        return requests.get(treatmentPlanRequestUrl + requestId, headers=headers)
    except requests.exceptions.ConnectionError:
        raise Exception(adsConnectionErr)

def requestStatusLoop(authToken, requestId, returnOnceInProcessFlag=False):

    while (True):
        res = requestStatus(authToken, requestId)
        status = res.json()[key_status]
        print(status)
        if status == status_inProcess and returnOnceInProcessFlag:
            return res
        if (status != status_completed and status != status_error):
            time.sleep(1)
            continue
        else:
            return res


def requestAndWaitForLastStatus(authToken, dmsName, patientId, syncTimestamps, lastStatus):
    res = treatmentPlanRequest(authToken, dmsName, patientId, syncTimestamps)
    requestId = res.json()[key_requestId]
    res.close()
    res = requestStatusLoop(authToken, requestId)
    assert res.json()[key_status] == lastStatus
    res.close()
    return requestId

async def requestAndGetInputFile(authToken, dmsName, patientId, syncTimestamps):
    res = treatmentPlanRequest(authToken, dmsName, patientId, syncTimestamps)
    requestId = res.json()[key_requestId]
    res.close()
    requestStatusLoop(testClientToken, requestId, True)
    return await getAlgoInputJson(requestId)

def requestRecommendation(authToken, requestId):
    headers = {'authorization': authToken}
    try:
        return requests.get(treatmentPlanRequestUrl + requestId +'/recommendation', headers=headers)
    except requests.exceptions.ConnectionError:
        raise Exception(adsConnectionErr)

def sendFeedback(authToken, requestId, feedback):
    headers = {'authorization': authToken,
               'Content-Type': 'application/json'}
    try:
        return requests.post(treatmentPlanRequestUrl + requestId +'/feedback', headers=headers, json=feedback)
    except requests.exceptions.ConnectionError:
        raise Exception(adsConnectionErr)