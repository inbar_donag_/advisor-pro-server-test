import requests
from constants import mockConnectionErr
from main import mock1


def getNotify(requestId, mock = mock1):

    mockUrl = "http://" + mock["host"] + ":" + mock["port"]
    headers = {'Content-Type': 'application/json'}
    try:
        return requests.get(mockUrl + '/getNotify', headers=headers, json={"requestId": requestId})
    except requests.exceptions.ConnectionError:
        raise Exception(mockConnectionErr)