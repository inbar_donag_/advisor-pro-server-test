import json

with open("./config/testsConfig.json") as json_data:
    config = json.load(json_data)
serverPath = config["serverPath"]

# SyncTimestamps

syncTimestamps1 = {
    "meter": "2016-12-24T12:00:00.000Z",
    "cgm": "2016-12-24T12:00:00.000Z",
    "pump": "2016-12-24T12:00:00.000Z"
}

syncTimestamps2 = {
    "meter": "2017-11-06T12:00:00.000Z",
    "cgm": "2017-01-01T12:00:00.000Z",
    "pump": "2017-01-01T12:00:00.000Z"
}

syncTimestamps3 = {
    "meter": "2017-01-31T20:21:10.000Z",
    "cgm": "2017-02-01T05:55:10.000Z",
    "pump": "2017-01-31T20:22:13.000Z"
}

syncTimestamps4 = {
    "meter": "2018-01-21T14:19:06.198Z",
    "cgm": "2018-01-21T14:22:03.000Z",
    "pump": "2018-01-21T14:19:06.198Z"
}

invalidSyncTimestamps = {
    "meter": "2014-11-33T12:00:00.000Z",
    "cgm": "2014-01-01T12:00:00.000Z",
    "pump": "2014-01-01T12:00:00.000Z"
}

badSyncTimestamps = {
    "meter": "22222",
    "cgm": "33333",
    "pump": "44444"
}

# DMS names

testDMS = "testDMS"
glooko = "glooko"

# patient IDs

validUser = "validUser"
invalidUser = "invalidUser"
fiveErrorsUser = "fiveErrorsUser"

# ADS error messages

invalidTimestampsErr = 'Invalid value for `syncTimestamps.meter`: '+ invalidSyncTimestamps["meter"]
requestNotFoundErr = 'error: Treatment plan request not found code=100'
outputCreationErr = 'Can\'t create output'
notAuthorizedErr = 'Not authorized to access this resource'

# ADS tests error messages

adsConnectionErr = "error: couldn't connect to ADS, maybe it's not running? no network?"
mockConnectionErr = "error: couldn't connect to Mock server, maybe it's not running? no network?"
logNameErr = "error: incorrect name of log file"

# log file names

accessLog = "access"
serverLog = "server"

# users jsons key names

user_pumpsSettings = "pumpsSettings"
user_pumpsReadings = "pumpsReadings"
user_pumpsNormalBoluses = "pumpsNormalBoluses"
user_pumpsExtendedBoluses = "pumpsExtendedBoluses"
user_pumpsScheduledBasals = "pumpsScheduledBasals"
user_pumpsSuspendBasals = "pumpsSuspendBasals"
user_pumpsTemporaryBasals = "pumpsTemporaryBasals"
user_pumpsEvents = "pumpsEvents"
user_cgmReadings = "cgmReadings"
user_readings = "readings"

# keys

key_requestId = "requestId"
key_status = "status"
key_code = "code"
key_message = "message"
key_data = "data"
key_error = "error"
key_returnCode = "returnCode"
key_meta = "meta"
key_rawSensor = "rawSensor"
key_bloodGlucose = "bloodGlucose"
key_recommendation = "recommendation"
key_recommendationError = "recommendation error"
key_rationale = "rationale"
key_hyperEvents = "hyperEvents"
key_hypoEvents = "hypoEvents"
key_general = "general"
key_basal = "basal"
key_segments = "segments"
key_insulinSensitivityFactor = "insulinSensitivityFactor"
key_insulinToCarbRatio = "insulinToCarbRatio"
key_basalRate = "basalRate"
key_patientID = "patientID"
key_feedback = "feedback"

# requests statuses

status_completed = 'COMPLETED'
status_error = 'ERROR'
status_inProcess ='IN_PROCESS'

# ads paths

algoPath = serverPath + 'advisorPro/AdvisorPro'
outputFolderPath = serverPath + 'advisorPro/output/'
inputFolderPath = serverPath + 'advisorPro/input/'
logsPath = serverPath + 'logs/'