import datetime
from scp import SCPClient
from constants import *
from main import ssh_client, downloadedFilesLocation

scp = SCPClient(ssh_client.get_transport())

async def getLogFile(type):

    if type != serverLog and type != accessLog:
        raise Exception(logNameErr)

    date = datetime.datetime.now().isoformat()[:10]
    localFile = downloadedFilesLocation + date + '.' + type + '.log'
    remoteFile = logsPath + date + '.' + type + '.log'
    scp.get(remoteFile,localFile)
    scp.close()
    with open(localFile, 'r') as file:
        return file.read()


async def getAlgoInputJson(requestId):

    localFile = downloadedFilesLocation + requestId + '_input.json'
    remoteFile = inputFolderPath + 'AdvisorPro_req_' + requestId + '.json'
    scp.get(remoteFile, localFile)
    scp.close()
    with open(localFile, 'r') as file:
        return json.load(file)

async def getAlgoOutputJson(requestId):

    localFile = downloadedFilesLocation + requestId + '_output.json'
    remoteFile = outputFolderPath + 'AdvisorPro_reco_' + requestId + '.json'
    scp.get(remoteFile, localFile)
    scp.close()
    with open(localFile, 'r') as file:
        return json.load(file)

def getUserFile(userFileName):
    with open('users/' + userFileName + '.json', 'r') as file:
        return json.load(file)