from constants import *
from filesHelper import getLogFile


async def assertInvalidTimestampsErr():
    log = await getLogFile(serverLog)
    assert log.__contains__(invalidTimestampsErr)

async def assertRequestNotFoundErr():
    log = await getLogFile(serverLog)
    assert log.__contains__(requestNotFoundErr)