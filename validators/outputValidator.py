from constants import *


def checkRationale(db_reco, output_reco):
    for k in db_reco[key_rationale].keys():
        if k in output_reco.keys():
            assert output_reco[k][key_rationale] == db_reco[key_rationale][k]

def checkSegments(key, db_reco, output_reco):
    i=0
    for s in db_reco[key][key_segments]:
        assert s == output_reco[key][key_segments][0][i]
        i+=1

def checkOutputMatchDB(db_reco, output_reco):
    checkRationale(db_reco, output_reco)
    checkSegments(key_insulinSensitivityFactor, db_reco, output_reco)
    checkSegments(key_insulinToCarbRatio, db_reco, output_reco)
    checkSegments(key_basalRate, db_reco, output_reco)
