from constants import *


def validateBasals(inputData, patientData): #TODO
    basals = []
    #map(lambda i: i["type"] = , patientData[])


def validateEvents(inputData, patientData):
    pass #TODO


def validateMapping(inputData, patientData):
    validateBasals(inputData, patientData)
    validateEvents(inputData, patientData)
    #TODO
    # validateTempBasal(inputData, patientData)
    # validateSuspendBasal(inputData, patientData)
    # validateCGM(inputData, patientData)
    # validateSettings(inputData, patientData)
    # validateUserAndPumpReadings(inputData, patientData)
    # validateNormalAndExtendedBolus(inputData, patientData)

def validateConstants(inputData):
    assert inputData["nightTimeGlucoseLow"] == 90
    assert inputData["nightTimeGlucoseHigh"] == 130
    assert inputData["dayTimeGlucoseLow"] == 80
    assert inputData["dayTimeGlucoseHigh"] == 110
    assert inputData["hypoThreshold"] == 70
    assert inputData["hyperThreshold"] == 180
    assert inputData["basalPercentTDD"] == 0.5
    assert inputData["BGGoal"] == 154

def validateInput(inputData, patientData):

    validateConstants(inputData)
    #TODO validate endTime
    validateMapping(inputData, patientData)

