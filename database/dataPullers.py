from main import db_client

advisorRequestsCollection = db_client.get_collection('advisorrequests')

def getRequest(requestId):
    return advisorRequestsCollection.find_one({"_sid": requestId})