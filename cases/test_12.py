import pytest
import requests

from constants import *
from serverRequests.adsRequests import host, port, treatmentPlanRequestUrl, getMessageForTreatmentPlanRequest, \
    testClientToken


class Test_12():

    invalidUrl = "http://" + host + ":" + port + '/api/v1/advisorPro/treatmentPlanRequestblablabla'
    headers = {'authorization': testClientToken,
               'Content-Type': 'application/json'}
    validMessage = getMessageForTreatmentPlanRequest(glooko, validUser, syncTimestamps2)
    badValuesMessage = getMessageForTreatmentPlanRequest(glooko, validUser, badSyncTimestamps)

    async def test_requestNotImplemented(self):
        try:
            res = requests.get(treatmentPlanRequestUrl, headers=self.headers)
            assert res.status_code == 404
            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)

    @pytest.mark.BVT
    async def test_resourceNotImplemented(self):
        try:
            res = requests.post(self.invalidUrl, headers=self.headers, json=self.validMessage)
            assert res.status_code == 404
            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)

    async def test_requestWithCutBody(self):
        try:
            res = requests.post(treatmentPlanRequestUrl, headers=self.headers, json={"dmsName": glooko})
            assert res.status_code == 400
            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)

    async def test_bodyRequestBadValues(self):
        try:
            res = requests.post(treatmentPlanRequestUrl, headers=self.headers, json=self.badValuesMessage)
            assert res.status_code == 400
            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)

    async def test_missingData(self):
        try:
            res = requests.post(treatmentPlanRequestUrl, headers=self.headers, json={"userId": validUser})
            assert res.status_code == 400
            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)