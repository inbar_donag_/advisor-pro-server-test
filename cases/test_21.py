import pytest
from constants import *
from filesHelper import getUserFile
from serverRequests.adsRequests import requestAndGetInputFile, testClientToken
from validators.inputValidator import validateInput


class Test_21():

    @pytest.mark.BVT
    async def test_mappingTest(self):
        user = 'STP21'
        patientData = getUserFile(user)
        inputData = await requestAndGetInputFile(testClientToken, glooko, user, syncTimestamps1)
        assert user == inputData[key_patientID]
        validateInput(inputData, patientData)