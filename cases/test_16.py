import pytest

from constants import *
from main import ValidVars
from serverRequests.adsRequests import requestRecommendation, testClientToken, treatmentPlanRequest, requestStatusLoop
from validators.errorsValidator import assertRequestNotFoundErr


class Test_16():

    @pytest.mark.BVT
    async def test_retrieveValidRequestRecommendation(self):
        res = requestRecommendation(testClientToken, ValidVars.getInstance().getValidID())
        assert key_data in res.json().keys()
        res.close()

    async def test_retrieveInvalidRequestRecommendation(self):
        res = requestRecommendation(testClientToken, 'blablabla')
        res.close()
        await assertRequestNotFoundErr()

    @pytest.mark.BVT
    async def test_requestAndRetrieveRecommendation(self):
        res = requestRecommendation(testClientToken, ValidVars.getInstance().getValidID(glooko))
        assert key_data in res.json().keys()
        res.close()

    async def test_statusTest(self):
        res1 = treatmentPlanRequest(testClientToken, glooko, validUser, syncTimestamps1)
        requestId1 = res1.json()[key_requestId]
        res1.close()
        res2 = treatmentPlanRequest(testClientToken, glooko, validUser, syncTimestamps1)
        requestId2 = res2.json()[key_requestId]
        res2.close()
        res1 = requestStatusLoop(testClientToken, requestId1, True)
        assert res1.json()[key_status] == status_inProcess
        res1.close()
        res2 = requestStatusLoop(testClientToken, requestId2, True)
        assert res2.json()[key_status] == status_inProcess
        res2.close()