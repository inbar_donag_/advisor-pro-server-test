import os
from multiprocessing.dummy import Process

from constants import *
from main import mock1, mock2
from serverRequests.adsRequests import testClientToken, testClient2Token, requestAndWaitForLastStatus, \
    requestRecommendation
from serverRequests.mockRequests import getNotify


class Test_17():

    async def test_sendTwoRequestFromTwoDifferentDMS(self):
        p1 = Process(target=self.sendRequest, args=(testClientToken, testClient2Token, mock1))
        p1.start()
        p2 = Process(target=self.sendRequest, args=(testClient2Token, testClientToken, mock2))
        p2.start()
        p1.join()
        p2.join()
        print('processes joined')


    def sendRequest(self, token1, token2, mock):
        print(os.getpid())
        requestId = requestAndWaitForLastStatus(token1, glooko, validUser, syncTimestamps1, status_completed)
        res1 = requestRecommendation(token2, requestId)
        assert res1.json()[key_message] == notAuthorizedErr
        assert res1.json()[key_code] == 1020
        res1.close()
        res2 = getNotify(requestId, mock)
        assert res2.json()[key_requestId] == requestId
        assert res2.json()[key_status] == status_completed
        res2.close()
