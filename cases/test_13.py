import json

import pytest

from constants import *
from database.dataPullers import getRequest
from filesHelper import getUserFile
from main import ValidVars
from serverRequests.adsRequests import requestAndWaitForLastStatus, testClientToken


class Test_13():

    @pytest.mark.BVT
    async def test_validRequestStoring(self):
        validID = ValidVars.getInstance().getValidID()
        reqKeys = getRequest(validID).keys()
        assert key_data in reqKeys
        assert key_error not in reqKeys

    async def test_invalidRequestStoring(self):
        requestId = requestAndWaitForLastStatus(testClientToken, testDMS, "blablabla", syncTimestamps2, status_error)
        reqKeys = getRequest(requestId).keys()
        assert key_data not in reqKeys
        assert key_error in reqKeys

    async def test_dataWithErrorsStore(self):
        # fiveErrorsUser has 2 errors in cgmReadings and 3 errors in readings
        requestId = requestAndWaitForLastStatus(testClientToken, testDMS, fiveErrorsUser, syncTimestamps2, status_error)
        data = getRequest(requestId)[key_data]
        rawSensor = data[key_rawSensor]
        bg = data[key_bloodGlucose]
        patientData = getUserFile(fiveErrorsUser)
        # assert data is stored without 5 errors
        assert len(rawSensor) == len(patientData[user_cgmReadings]) - 2
        assert len(bg) == len(patientData[user_readings]) - 3

    async def test_notEnoughDataStore(self):
        # invalidUser has only 3 cgmReadings
        requestId = requestAndWaitForLastStatus(testClientToken, testDMS, invalidUser, syncTimestamps2, status_error)
        reqKeys = getRequest(requestId).keys()
        assert key_data in reqKeys
        assert key_error in reqKeys