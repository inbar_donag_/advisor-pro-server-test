import pytest
from constants import *
from database.dataPullers import getRequest
from main import ValidVars


class Test_14():

    @pytest.mark.BVT
    async def test_validDMSRequest(self):
        requestId = ValidVars.getInstance().getValidID(glooko)
        reqKeys = getRequest(requestId).keys()
        assert key_data in reqKeys
        assert key_error not in reqKeys