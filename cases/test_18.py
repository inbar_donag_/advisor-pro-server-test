import pytest
from constants import *
from main import ssh_client
from serverRequests.adsRequests import testClientToken, requestAndWaitForLastStatus, requestAndGetInputFile


class Test_18():
    @pytest.mark.BVT
    @pytest.mark.run(order=-1)
    async def test_run15Requests(self):
        for i in list(range(0,15)):
            await self.runRequest()

    def test_executeThatCantBeRun(self):
        oldFile = algoPath
        newFile = algoPath + '_2'
        ssh_client.exec_command('mv '+ oldFile + ' ' + newFile)
        ssh_client.exec_command('touch ' + oldFile)
        requestAndWaitForLastStatus(testClientToken, glooko, validUser, syncTimestamps1, status_error)
        ssh_client.exec_command('mv ' + newFile + ' ' + oldFile)

    async def runRequest(self):
        input = await requestAndGetInputFile(testClientToken, glooko, validUser, syncTimestamps1)
        assert input[key_patientID] == validUser