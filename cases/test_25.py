import pytest
from constants import *
from database.dataPullers import getRequest
from main import ValidVars
from serverRequests.adsRequests import sendFeedback, testClientToken
from validators.feedbackValidator import checkFeedbackMatchDB


class Test_25():

    feedback = {
        "createdAt": "2017-06-22T13:04:00.000Z",
        "physicianId": 'aaaaaaa',
        "physicianName": 'Doctor p',
        "sharedWithPatient": True,
        "data": {
            "basalRate": {
                "segments": [
                    {
                        "start": 0,
                        "value": 1.5
                    },
                    {
                        "start": 60,
                        "value": 1.5
                    },
                    {
                        "start": 120,
                        "value": 1.5
                    },
                    {
                        "start": 180,
                        "value": 1.3
                    },
                    {
                        "start": 240,
                        "value": 1.3
                    },
                    {
                        "start": 300,
                        "value": 1.3
                    },
                    {
                        "start": 360,
                        "value": 1.15
                    },
                    {
                        "start": 420,
                        "value": 1.2
                    },
                    {
                        "start": 480,
                        "value": 1.2
                    },
                    {
                        "start": 540,
                        "value": 1.2
                    },
                    {
                        "start": 600,
                        "value": 1.25
                    },
                    {
                        "start": 660,
                        "value": 1.25
                    },
                    {
                        "start": 720,
                        "value": 1.25
                    },
                    {
                        "start": 780,
                        "value": 1.25
                    },
                    {
                        "start": 840,
                        "value": 1.3
                    },
                    {
                        "start": 900,
                        "value": 1.45
                    },
                    {
                        "start": 960,
                        "value": 1.45
                    },
                    {
                        "start": 1020,
                        "value": 1.45
                    },
                    {
                        "start": 1080,
                        "value": 1.55
                    },
                    {
                        "start": 1140,
                        "value": 1.55
                    },
                    {
                        "start": 1200,
                        "value": 1.55
                    },
                    {
                        "start": 1260,
                        "value": 1.55
                    },
                    {
                        "start": 1320,
                        "value": 1.55
                    },
                    {
                        "start": 1380,
                        "value": 1.6
                    }
                ],
                "rationale": [
                    "rationale 1",
                    "rationale 2"
                ]
            },
            "insulinSensitivityFactor": {
                "segments": [
                    {
                        "start": 0,
                        "value": 44
                    },
                    {
                        "start": 360,
                        "value": 39
                    },
                    {
                        "start": 660,
                        "value": 39
                    },
                    {
                        "start": 900,
                        "value": 39
                    },
                    {
                        "start": 1320,
                        "value": 44
                    }
                ]
            },
            "insulinToCarbRatio": {
                "segments": [
                    {
                        "start": 0,
                        "value": 6.7
                    },
                    {
                        "start": 360,
                        "value": 4.9
                    },
                    {
                        "start": 660,
                        "value": 4.7
                    },
                    {
                        "start": 900,
                        "value": 6.5
                    },
                    {
                        "start": 1320,
                        "value": 6.7
                    }
                ]
            },
            "rationale": {
                "hyperEvents": ["rationale 1"],
                "hypoEvents": ["rationale 1"],
                "general": ["rationale 1"]
            }
        }
    }

    @pytest.mark.BVT
    async def test_validFeedbackDBStoring(self):
        validID = ValidVars.getInstance().getValidID()
        sendFeedback(testClientToken, validID, self.feedback)
        req = getRequest(validID)
        assert key_feedback in req.keys()
        checkFeedbackMatchDB(self.feedback, req[key_feedback][0])