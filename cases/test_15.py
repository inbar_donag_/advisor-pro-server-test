import pytest
from constants import *
from database.dataPullers import getRequest
from filesHelper import getAlgoOutputJson
from main import ValidVars
from serverRequests.adsRequests import requestAndWaitForLastStatus, testClientToken
from validators.outputValidator import checkOutputMatchDB


class Test_15():

    @pytest.mark.BVT
    async def test_validUserRecommendationStoring(self):
        validID = ValidVars.getInstance().getValidID()
        reqKeys = getRequest(validID).keys()
        assert key_recommendation in reqKeys

    @pytest.mark.BVT
    async def test_invalidUserRecommendationStoring(self):
        requestId = requestAndWaitForLastStatus(testClientToken, glooko, invalidUser, syncTimestamps2, status_error)
        reqKeys = getRequest(requestId).keys()
        assert key_recommendation not in reqKeys
        assert key_error in reqKeys

    async def test_verifyOutputMatchToDB(self):
        validID = ValidVars.getInstance().getValidID()
        db_reco = getRequest(validID)[key_recommendation][key_data]
        output_reco = await getAlgoOutputJson(validID)
        checkOutputMatchDB(db_reco, output_reco)
