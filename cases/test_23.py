import pytest
import requests
from constants import *
from serverRequests.adsRequests import treatmentPlanRequestUrl, getMessageForTreatmentPlanRequest, treatmentPlanRequest


class Test_23():

    @pytest.mark.BVT
    async def test_sendRequestWithValidToken(self):
        pass #already checked in conftest.py

    async def test_sendRequestWithNoToken(self):
        headers = {'Content-Type': 'application/json'}
        try:
            res = requests.post(treatmentPlanRequestUrl, headers=headers,
                                 json=getMessageForTreatmentPlanRequest(glooko, validUser, syncTimestamps2))
            assert res.json()[key_message] == notAuthorizedErr

            res.close()
        except requests.exceptions.ConnectionError:
            raise Exception(adsConnectionErr)

    async def test_sendRequestWithEmptyToken(self):
        res = treatmentPlanRequest("", glooko, validUser, syncTimestamps1)
        assert res.json()[key_message] == notAuthorizedErr
        res.close()

    async def test_createClientAndSendRequestWithTheToken(self):
        pass #TODO