import pytest

from constants import *
from main import ssh_client
from serverRequests.adsRequests import treatmentPlanRequest, testClientToken, requestStatusLoop


class Test_19():
    @pytest.mark.BVT
    async def test_validInputTest(self):
        pass #already checked in test_requestAndRetrieveRecommendation()

    @pytest.mark.BVT
    def test_errorResponseTest(self):
        res = treatmentPlanRequest(testClientToken, glooko, validUser, syncTimestamps1)
        requestId = res.json()[key_requestId]
        res.close()
        requestStatusLoop(testClientToken, requestId, True)
        ssh_client.exec_command('chmod 0 ' + outputFolderPath)
        res = requestStatusLoop(testClientToken, requestId)
        ssh_client.exec_command('chmod 777 ' + outputFolderPath)
        resBody = res.json()
        assert resBody[key_status] == status_error
        assert resBody[key_error][key_meta][key_message] == outputCreationErr
        assert resBody[key_error][key_meta][key_returnCode] == 50
        res.close()

