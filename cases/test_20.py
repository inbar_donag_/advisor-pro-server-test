from main import ValidVars
from constants import *
from serverRequests.mockRequests import getNotify


class Test_20():

    async def test_checkValidRequestNotify(self):
        requestId = ValidVars.getInstance().getValidID(glooko)
        res = getNotify(requestId)
        assert res.json()[key_requestId] == requestId
        assert res.json()[key_status] == status_completed
        res.close()
