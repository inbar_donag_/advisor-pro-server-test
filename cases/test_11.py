import pytest
from constants import *
from serverRequests.adsRequests import treatmentPlanRequest, testClientToken
from validators.errorsValidator import assertInvalidTimestampsErr


class Test_11():
    @pytest.mark.BVT
    async def test_sameIdRequestIdCheck(self):

        res1 = treatmentPlanRequest(testClientToken, testDMS, validUser, syncTimestamps2)
        assert res1.status_code == 202
        firstRequestId = res1.json()[key_requestId]
        res1.close()
        res2 = treatmentPlanRequest(testClientToken, testDMS, validUser, syncTimestamps2)
        assert res2.status_code == 202
        secondRequestId = res2.json()[key_requestId]
        res2.close()
        assert firstRequestId != secondRequestId

    @pytest.mark.BVT
    async def test_invalidTimestamp(self):
        res = treatmentPlanRequest(testClientToken, testDMS, validUser, invalidSyncTimestamps)
        await assertInvalidTimestampsErr()
        assert key_requestId not in res.json()
        res.close()
